#pragma once

#include <QMainWindow>
#include "task.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
    void addTask();
    void removeTask(Task*);
    void taskStatusChanged(Task*);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void updateStatus();

private:
    Ui::MainWindow *ui;
    QVector<Task*> _tasks;
};
