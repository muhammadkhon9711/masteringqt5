#pragma once

#include <QWidget>

namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT
public slots:
    void rename();
private slots:
    void checked(bool checked);
public:
    explicit Task(const QString& name, QWidget *parent = nullptr);
    ~Task();

    void setName(const QString& name);
    QString name() const;
    bool isCompleted() const;
signals:
    void removed(Task* task);
    void statusChanged(Task* task);
protected:
    void changeEvent(QEvent *e);

private:
    Ui::Task *ui;
};

