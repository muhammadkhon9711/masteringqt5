#include "task.hpp"
#include "ui_task.h"
#include <QInputDialog>
#include <QDebug>

void Task::rename()
{
    bool ok;
    auto value = QInputDialog::getText(this, tr("Edit task"),
                                       tr("Task name"),
                                       QLineEdit::Normal,
                                       this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::checked(bool checked)
{
    auto font{ui->checkbox->font()};
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);
    emit statusChanged(this);
}

Task::Task(const QString &name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);
    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
    connect(ui->removeButton, &QPushButton::clicked, [this, name] {
        qDebug() << "Trying to remove" << name;
        emit removed(this);
    });
}

Task::~Task()
{
    delete ui;
}

void Task::setName(const QString &name)
{
    ui->checkbox->setText(name);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
    return ui->checkbox->isChecked();
}

void Task::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
