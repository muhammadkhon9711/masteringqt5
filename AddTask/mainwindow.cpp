#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QInputDialog>

void MainWindow::addTask()
{
    bool ok;
    auto name = QInputDialog::getText(this,
                                      tr("Add task"),
                                      tr("Task name"),
                                      QLineEdit::Normal,
                                      tr("Untitled task"),
                                      &ok);
    if (ok && !name.isEmpty()) {
        qDebug() << "Adding new task";
        auto task = new Task(name);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        _tasks.push_back(task);
        ui->tasksLayout->addWidget(task);

        updateStatus();
    }
}

void MainWindow::removeTask(Task *task)
{
    _tasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    task->setParent(nullptr);
    delete task;

    updateStatus();
}

void MainWindow::taskStatusChanged(Task *)
{
    updateStatus();
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
    _tasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);

    updateStatus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateStatus()
{
    auto completedCount = 0;
    for (const auto& t: _tasks) {
        if (t->isCompleted()) {
            ++completedCount;
        }
    }

    auto todoCount = _tasks.size() - completedCount;
    ui->statusLabel->setText(QString("Status: %1 todo / %2 completed")
                                 .arg(todoCount)
                                 .arg(completedCount));
}

