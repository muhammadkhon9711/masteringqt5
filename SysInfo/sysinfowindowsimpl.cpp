#include "sysinfowindowsimpl.hpp"
#include <windows.h>

SysInfoWindowsImpl::SysInfoWindowsImpl() : SysInfo(),
    _cpuLoadLastValues()
{

}

void SysInfoWindowsImpl::init()
{
    _cpuLoadLastValues = cpuRawData();
}

double SysInfoWindowsImpl::cpuLoadAverage()
{
    auto firstSample   = _cpuLoadLastValues;
    auto secondSample  = cpuRawData();
    _cpuLoadLastValues = secondSample;

    auto currentIdle    = secondSample[0] - firstSample[0];
    auto currentKernel  = secondSample[1] - firstSample[1];
    auto currentUser    = secondSample[2] - firstSample[2];
    auto currentSystem  = currentKernel - currentUser;

    double percent = (currentSystem - currentIdle) * 100.0 / currentSystem;
    return qBound(0.0, percent, 100.0);
}

double SysInfoWindowsImpl::memoryUsed()
{
    MEMORYSTATUSEX memoryStatus;
    memoryStatus.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memoryStatus);
    auto memoryPhysicalUsed = memoryStatus.ullTotalPhys - memoryStatus.ullAvailPhys;
    return static_cast<double>(memoryPhysicalUsed * 100.0 / memoryStatus.ullTotalPhys);
}

SysInfoWindowsImpl::Data SysInfoWindowsImpl::cpuRawData()
{
    FILETIME idleTime;
    FILETIME kernelTime;
    FILETIME userTime;

    GetSystemTimes(&idleTime, &kernelTime, &userTime);

    Data rawData;

    rawData.push_back(convertFileTime(idleTime));
    rawData.push_back(convertFileTime(kernelTime));
    rawData.push_back(convertFileTime(userTime));
    return rawData;
}

quint64 SysInfoWindowsImpl::convertFileTime(const FILETIME &filetime) const
{
    ULARGE_INTEGER largeInteger;
    largeInteger.LowPart  = filetime.dwLowDateTime;
    largeInteger.HighPart = filetime.dwHighDateTime;
    return largeInteger.QuadPart;
}
