#include "cpuwidget.hpp"
#include "sysinfo.hpp"

using namespace QtCharts;

CpuWidget::CpuWidget(QWidget *parent) : SysInfoWidget(parent),
    _series(new QPieSeries(this))
{
    _series->setHoleSize(0.35);
    _series->append("CPU Load", 30.0);
    _series->append("CPU Free", 70.0);

    auto chart = chartView().chart();
    chart->addSeries(_series);
    chart->setTitle("CPU average load");
}

void CpuWidget::updateSeries()
{
    auto cpuLoadAverage = SysInfo::instance().cpuLoadAverage();
    _series->clear();
    _series->append("Load", cpuLoadAverage);
    _series->append("Free", 100.1 - cpuLoadAverage);
}
