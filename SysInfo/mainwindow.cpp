#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "sysinfo.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
    _cpuWidget(this),
    _memoryWidget(this)
{
    ui->setupUi(this);
    SysInfo::instance().init();
    ui->centralwidget->layout()->addWidget(&_cpuWidget);
    ui->centralwidget->layout()->addWidget(&_memoryWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

