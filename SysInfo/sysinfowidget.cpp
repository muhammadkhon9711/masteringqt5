#include "sysinfowidget.hpp"
#include <QVBoxLayout>

using namespace QtCharts;

SysInfoWidget::SysInfoWidget(QWidget *parent,
                             int startDelayMs,
                             int updateSeriesDelayMs) : QWidget(parent),
    _chartView(this)
{
    _refreshTimer.setInterval(updateSeriesDelayMs);
    connect(&_refreshTimer, &QTimer::timeout, this, &SysInfoWidget::updateSeries);
    QTimer::singleShot(startDelayMs, [this] { _refreshTimer.start(); });

    _chartView.setRenderHint(QPainter::Antialiasing);
    _chartView.chart()->legend()->setVisible(false);
    auto layout = new QVBoxLayout(this);
    layout->addWidget(&_chartView);
    setLayout(layout);
}

QtCharts::QChartView &SysInfoWidget::chartView()
{
    return _chartView;
}
