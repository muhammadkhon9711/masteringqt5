#pragma once

#include <QtGlobal>
#include <QVector>
#include "sysinfo.hpp"

typedef struct _FILETIME FILETIME;

class SysInfoWindowsImpl : public SysInfo
{
public:
    SysInfoWindowsImpl();

    // SysInfo interface
public:
    using Data = QVector<quint64>;
    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;
private:
    Data cpuRawData();
    Data _cpuLoadLastValues;
    quint64 convertFileTime(const FILETIME &filetime) const;
};

