#include "memorywidget.hpp"
#include <QAreaSeries>
#include "sysinfo.hpp"
using namespace QtCharts;

constexpr auto CHART_X_RANGE_COUNT  = 50;
constexpr auto CHART_X_RANGE_MAX    = CHART_X_RANGE_COUNT - 1;

MemoryWidget::MemoryWidget(QWidget *parent) : SysInfoWidget(parent),
    _series(new QLineSeries(this)),
    _pointPositionX(0)
{
    auto areaSeries = new QAreaSeries(_series);
    auto chart = chartView().chart();
    chart->addSeries(areaSeries);
    chart->setTitle("Memory used");
    chart->createDefaultAxes();
    chart->axisX()->setVisible(false);
    chart->axisX()->setRange(0, CHART_X_RANGE_MAX);
    chart->axisY()->setRange(0, 100);

}

void MemoryWidget::updateSeries()
{
    auto memoryUsed = SysInfo::instance().memoryUsed();
    _series->append(_pointPositionX++, memoryUsed);
    if (_series->count() > CHART_X_RANGE_COUNT) {
        auto chart = chartView().chart();
        chart->scroll(chart->plotArea().width() / CHART_X_RANGE_MAX, 0);
        _series->remove(0);
    }
}
