#pragma once

#include <QMainWindow>
#include "cpuwidget.hpp"
#include "memorywidget.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    CpuWidget _cpuWidget;
    MemoryWidget _memoryWidget;
};
