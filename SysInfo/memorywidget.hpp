#pragma once

#include "sysinfowidget.hpp"
#include <QLineSeries>

class MemoryWidget : public SysInfoWidget
{
public:
    explicit  MemoryWidget(QWidget* parent = nullptr);

    // SysInfoWidget interface
protected:
    void updateSeries() override;
private:
    QtCharts::QLineSeries *_series;
    qint64 _pointPositionX;
};

