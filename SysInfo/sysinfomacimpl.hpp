#pragma once

#include <sysinfo.hpp>

class SysInfoMacImpl : public SysInfo
{
public:
    SysInfoMacImpl();

    // SysInfo interface
public:
    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;
};

