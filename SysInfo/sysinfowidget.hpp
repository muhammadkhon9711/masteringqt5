#pragma once

#include <QWidget>
#include <QTimer>
#include <QChartView>

class SysInfoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SysInfoWidget(QWidget *parent = nullptr,
                           int = 500, int = 500);
protected:
    QtCharts::QChartView& chartView();
    virtual void updateSeries() = 0;
private:
    QTimer _refreshTimer;
    QtCharts::QChartView _chartView;
};

