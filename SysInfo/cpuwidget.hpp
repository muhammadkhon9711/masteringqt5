#pragma once

#include "sysinfowidget.hpp"
#include <QPieSeries>

class CpuWidget : public SysInfoWidget
{
public:
    explicit CpuWidget(QWidget* parent = nullptr);

    // SysInfoWidget interface
protected:
    void updateSeries() override;
private:
    QtCharts::QPieSeries* _series;
};

